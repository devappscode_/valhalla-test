import {UPDATE_POSTS, UPDATE_USERS} from './types';

export const updatePosts = payload => ({
  type: UPDATE_POSTS,
  payload: payload,
});

export const updateUsers = payload => ({
  type: UPDATE_USERS,
  payload: payload,
});
