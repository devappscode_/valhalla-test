import {createStore, combineReducers} from 'redux';
import {accountReducer, appReducer} from '../reducers/reducer';

const rootReducer = combineReducers({
  accountReducer: accountReducer,
  appReducer: appReducer,
});

const configureStore = () => createStore(rootReducer);

export default configureStore;
