import {UPDATE_POSTS} from '../actions/types';
import {UPDATE_USERS} from '../actions/types';

const initialState = {
  posts: [],
  users: [],
};

export const accountReducer = (state = initialState, action) => {
  let actionType = action.type;
  switch (actionType) {
    // case LOGIN:
    //   return {
    //     ...state,
    //     account: action.payload,
    //     isLogin: true,
    //   };

    default:
      return state;
  }
};

export const appReducer = (state = initialState, action) => {
  let actionType = action.type;
  switch (actionType) {
    case UPDATE_POSTS:
      return {
        ...state,
        posts: action.payload,
      };
    case UPDATE_USERS:
      return {
        ...state,
        users: action.payload,
      };
    default:
      return state;
  }
};
