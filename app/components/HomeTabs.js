import React from 'react';

import {StyleSheet, TouchableOpacity, View, Text, Image} from 'react-native';
import {Home, Search, Notifications, Message} from '../constants/icons';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../constants/theme';

const TabIcons = (isFocused, tabID) => {
  switch (tabID) {
    case 0:
      return isFocused ? (
        <Image source={Home} style={styles.activeIcon} resizeMode={'cover'} />
      ) : (
        <Image source={Home} style={styles.inactiveIcon} resizeMode={'cover'} />
      );
    case 1:
      return isFocused ? (
        <Image source={Search} style={styles.activeIcon} resizeMode={'cover'} />
      ) : (
        <Image
          source={Search}
          style={styles.inactiveIcon}
          resizeMode={'cover'}
        />
      );
    case 2:
      return isFocused ? (
        <Image
          source={Notifications}
          style={styles.activeIcon}
          resizeMode={'cover'}
        />
      ) : (
        <Image
          source={Notifications}
          style={styles.inactiveIcon}
          resizeMode={'cover'}
        />
      );
    case 3:
      return isFocused ? (
        <Image
          source={Message}
          style={styles.activeIcon}
          resizeMode={'cover'}
        />
      ) : (
        <Image
          source={Message}
          style={styles.inactiveIcon}
          resizeMode={'cover'}
        />
      );
    default:
  }
};

const HomeTabs = ({state, descriptors, navigation}) => {
  return (
    <View style={styles.tabContainer}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            style={{flex: 1, alignItems: 'center'}}>
            {TabIcons(isFocused, index)}
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  tabContainer: {
    flexDirection: 'row',
    height: DEVICE_HEIGHT * 0.08,
    justifyContent: 'center',
    backgroundColor: 'white',
    borderTopWidth: DEVICE_HEIGHT * 0.0008,
    borderTopColor: 'lightgray',
    alignItems: 'center',
  },
  activeIcon: {
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_WIDTH * 0.05,
    tintColor: '#1DA1F2',
  },
  inactiveIcon: {
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_WIDTH * 0.05,
    tintColor: 'lightgray',
  },
});

export default HomeTabs;
