import React from 'react';

import {StyleSheet, TouchableOpacity, View, Text, Image} from 'react-native';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../constants/theme';
import {User} from '../constants/icons';

const CommentCard = ({item}) => {
  const {name, email, body} = item;

  return (
    <TouchableOpacity
      onPress={() => console.log('Pressed')}
      style={styles.commentCard}>
      <View style={styles.userContainer}>
        <Image source={User} style={styles.userIcon} resizeMode={'cover'} />
        <View>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.username}>{email}</Text>
        </View>
      </View>

      <Text style={styles.body}>{body}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  commentCard: {
    width: DEVICE_WIDTH * 1,
    borderBottomWidth: DEVICE_HEIGHT * 0.0008,
    borderBottomColor: 'lightgray',
    paddingHorizontal: DEVICE_WIDTH * 0.03,
    paddingTop: DEVICE_HEIGHT * 0.01,
    paddingBottom: DEVICE_HEIGHT * 0.04,
  },
  userIcon: {
    width: DEVICE_WIDTH * 0.075,
    height: DEVICE_WIDTH * 0.075,
    marginRight: DEVICE_WIDTH * 0.03,
  },
  name: {
    fontSize: DEVICE_HEIGHT * 0.013,
    fontFamily: 'Montserrat-Bold',
  },
  username: {
    fontSize: DEVICE_HEIGHT * 0.013,
    fontFamily: 'Montserrat-Regular',
  },
  userContainer: {
    flexDirection: 'row',
    marginBottom: DEVICE_HEIGHT * 0.02,
  },
  body: {
    fontSize: DEVICE_HEIGHT * 0.012,
    fontFamily: 'Montserrat-Regular',
  },
});

export default CommentCard;
