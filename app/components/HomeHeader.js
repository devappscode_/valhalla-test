import React from 'react';

import {
  StyleSheet,
  TouchableOpacity,
  Platform,
  View,
  Text,
  Image,
  TextInput,
} from 'react-native';
import {Home, Search, Notifications, Message} from '../constants/icons';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../constants/theme';

const HomeHeader = ({onTextChange}) => {
  return (
    <View style={styles.homeHeader}>
      <View style={styles.searchContainer}>
        <TextInput
          onChangeText={text => onTextChange(text)}
          style={styles.searchInput}
        />
        <Image source={Search} style={styles.searchIcon} resizeMode={'cover'} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  homeHeader: {
    flexDirection: 'row',
    height: Platform.OS === 'ios' ? DEVICE_HEIGHT * 0.1 : DEVICE_HEIGHT * 0.08,
    width: DEVICE_WIDTH * 1,
    borderBottomWidth: DEVICE_HEIGHT * 0.0008,
    borderBottomColor: 'lightgray',
    alignItems: 'center',
    paddingTop:
      Platform.OS === 'ios' ? DEVICE_HEIGHT * 0.05 : DEVICE_HEIGHT * 0.02,
    paddingHorizontal: DEVICE_WIDTH * 0.05,
  },
  searchIcon: {
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_WIDTH * 0.05,
    tintColor: '#1DA1F2',
  },
  searchInput: {
    width: DEVICE_WIDTH * 0.8,
    height:
      Platform.OS === 'ios' ? DEVICE_HEIGHT * 0.035 : DEVICE_HEIGHT * 0.04,
    padding: 0,
  },
  searchContainer: {
    width: DEVICE_WIDTH * 0.9,
    height:
      Platform.OS === 'ios' ? DEVICE_HEIGHT * 0.035 : DEVICE_HEIGHT * 0.04,
    borderWidth: DEVICE_HEIGHT * 0.0008,
    borderColor: 'lightgray',
    borderRadius: DEVICE_WIDTH * 0.04,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});

export default HomeHeader;
