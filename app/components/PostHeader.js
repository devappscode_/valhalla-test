import React from 'react';

import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  Platform,
} from 'react-native';
import {Back, User} from '../constants/icons';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../constants/theme';
import {useNavigation} from '@react-navigation/native';

const PostHeader = ({username, name}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.postHeader}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={Back} style={styles.backIcon} resizeMode={'cover'} />
      </TouchableOpacity>
      <Image source={User} style={styles.userIcon} resizeMode={'cover'} />
      <View>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.username}>@{username}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  postHeader: {
    flexDirection: 'row',
    height: Platform.OS === 'ios' ? DEVICE_HEIGHT * 0.1 : DEVICE_HEIGHT * 0.08,
    width: DEVICE_WIDTH * 1,
    borderBottomWidth: DEVICE_HEIGHT * 0.0008,
    borderBottomColor: 'lightgray',
    alignItems: 'center',
    paddingTop:
      Platform.OS === 'ios' ? DEVICE_HEIGHT * 0.05 : DEVICE_HEIGHT * 0.02,
    paddingLeft: DEVICE_WIDTH * 0.03,
  },
  backIcon: {
    width: DEVICE_WIDTH * 0.06,
    height: DEVICE_WIDTH * 0.06,
    tintColor: '#1DA1F2',
    marginRight: DEVICE_WIDTH * 0.04,
  },
  userIcon: {
    width: DEVICE_WIDTH * 0.085,
    height: DEVICE_WIDTH * 0.085,
    marginRight: DEVICE_WIDTH * 0.015,
  },
  name: {
    fontSize: DEVICE_HEIGHT * 0.015,
    fontFamily: 'Montserrat-Bold',
  },
  username: {
    fontSize: DEVICE_HEIGHT * 0.015,
    fontFamily: 'Montserrat-Regular',
  },
});

export default PostHeader;
