import React from 'react';

import {StyleSheet, TouchableOpacity, View, Text, Image} from 'react-native';
import {Home, Search, Notifications, Message} from '../constants/icons';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../constants/theme';
import {User} from '../constants/icons';

const PostCard = ({item, userData, onPressPost}) => {
  const {id, userId, title, body} = item;
  const {name, username} = userData;

  return (
    <TouchableOpacity
      onPress={() => onPressPost(item, userData)}
      style={styles.postCard}>
      <View style={styles.userContainer}>
        <Image source={User} style={styles.userIcon} resizeMode={'cover'} />
        <View>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.username}>@{username}</Text>
        </View>
      </View>

      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  postCard: {
    width: DEVICE_WIDTH * 1,
    borderBottomWidth: DEVICE_HEIGHT * 0.0008,
    borderBottomColor: 'lightgray',
    paddingHorizontal: DEVICE_WIDTH * 0.03,
    paddingTop: DEVICE_HEIGHT * 0.01,
    paddingBottom: DEVICE_HEIGHT * 0.04,
  },
  userIcon: {
    width: DEVICE_WIDTH * 0.085,
    height: DEVICE_WIDTH * 0.085,
    marginRight: DEVICE_WIDTH * 0.03,
  },
  name: {
    fontSize: DEVICE_HEIGHT * 0.015,
    fontFamily: 'Montserrat-Bold',
  },
  username: {
    fontSize: DEVICE_HEIGHT * 0.015,
    fontFamily: 'Montserrat-Regular',
  },
  userContainer: {
    flexDirection: 'row',
    marginBottom: DEVICE_HEIGHT * 0.02,
  },
  title: {
    fontSize: DEVICE_HEIGHT * 0.015,
    fontFamily: 'Montserrat-SemiBold',
  },
});

export default PostCard;
