import React from 'react';

import {StyleSheet, View, Text} from 'react-native';

import {NavigationContainer, StackActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

// import OnBoardingStack from './navigation/OnBoardingStack.js';
import Notifications from './screens/MainTabs/Notifications.js';
import Home from './screens/MainTabs/Home.js';
import Search from './screens/MainTabs/Search.js';
import Messages from './screens/MainTabs/Messages.js';
import Post from './screens/PostScreens/Post.js';

import HomeTabs from './components/HomeTabs';

const mainTabStack = createBottomTabNavigator();
const homeStack = createStackNavigator();

const MainTabStackScreen = () => (
  <mainTabStack.Navigator
    tabBar={props => <HomeTabs {...props} />}
    tabBarOptions={{
      keyboardHidesTabBar: true,
    }}>
    <mainTabStack.Screen name="Home" component={HomeStackScreen} />
    <mainTabStack.Screen name="Search" component={Search} />
    <mainTabStack.Screen name="Notifications" component={Notifications} />
    <mainTabStack.Screen name="Messages" component={Messages} />
  </mainTabStack.Navigator>
);

const HomeStackScreen = () => (
  <homeStack.Navigator screenOptions={{headerShown: false}}>
    <homeStack.Screen name="Home" component={Home} />
    <homeStack.Screen name="Post" component={Post} />
  </homeStack.Navigator>
);

const App = () => {
  return (
    <NavigationContainer>
      <MainTabStackScreen />
    </NavigationContainer>
  );
};

export default App;
