export const Home = require('../assets/icons/home.png');
export const Search = require('../assets/icons/search.png');
export const Notifications = require('../assets/icons/notifications.png');
export const Message = require('../assets/icons/message.png');
export const User = require('../assets/icons/user.png');
export const Back = require('../assets/icons/back.png');

export default {Home, Search, Notifications, Message, User, Back};
