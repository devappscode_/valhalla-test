import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {getPosts, getUsers} from '../../api/index';
import PostCard from '../../components/PostCard';
import HomeHeader from '../../components/HomeHeader';
import {DEVICE_HEIGHT} from '../../constants/theme';
import {useNavigation} from '@react-navigation/native';
import {getUserInfo, searchPosts} from '../../helpers/helpers';
const Home = () => {
  const navigation = useNavigation();
  const posts = useSelector(state => state.appReducer.posts);
  const [results, setResults] = useState([]);
  const users = useSelector(state => state.appReducer.users);
  const dispatch = useDispatch();

  useEffect(() => {
    getPosts(dispatch);
    getUsers(dispatch);
  }, []);

  const onPressPost = (item, userData) => {
    navigation.navigate('Post', {item, userData});
  };

  const onTextChange = inputString => {
    let searchResults = searchPosts(inputString, users, posts);
    setResults(searchResults);
  };

  const renderPost = ({item, index}) => {
    let userData = getUserInfo(item.userId, users);
    return (
      <PostCard item={item} userData={userData} onPressPost={onPressPost} />
    );
  };
  return (
    <View style={styles.mainContainer}>
      <HomeHeader onTextChange={onTextChange} />

      {users.length !== 0 ? (
        <FlatList
          data={results.length === 0 ? posts : results}
          style={styles.flatList}
          renderItem={renderPost}
          keyExtractor={(item, index) => `${index}-${item.id}`}
          // decelerationRate={0.1}
        />
      ) : (
        <ActivityIndicator color="#1DA1F2" style={styles.loader} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    height: DEVICE_HEIGHT * 0.92,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  loader: {
    marginTop: DEVICE_HEIGHT * 0.03,
  },
});

export default Home;
