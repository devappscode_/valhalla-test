import React from 'react';

import {StyleSheet, View, Text} from 'react-native';

const Messages = () => {
  return (
    <View style={styles.mainContainer}>
      <Text>Messages</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Messages;
