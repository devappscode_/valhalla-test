import React from 'react';

import {StyleSheet, View, Text} from 'react-native';

const Search = () => {
  return (
    <View style={styles.mainContainer}>
      <Text>Search</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Search;
