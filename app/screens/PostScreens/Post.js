import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  TextInput,
  Text,
  FlatList,
  Platform,
  Image,
  ActivityIndicator,
} from 'react-native';
import {getComments} from '../../api/index';
import PostHeader from '../../components/PostHeader';
import CommentCard from '../../components/CommentCard';
import {User, Search} from '../../constants/icons';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../../constants/theme';
import {getUserInfo, searchComments} from '../../helpers/helpers';

const Post = props => {
  const {id, userId, title, body} = props.route.params.item;
  const {name, username} = props.route.params.userData;
  const [comments, setComments] = useState([]);
  const trimBreakLine = body.replace(/(\r\n|\n|\r)/gm, '');
  const users = useSelector(state => state.appReducer.users);
  const [results, setResults] = useState([]);

  useEffect(() => {
    async function init() {
      const result = await getComments(id);
      setComments(result);
    }

    init();
  }, []);

  const onTextChange = inputString => {
    let searchResults = searchComments(inputString, comments);
    setResults(searchResults);
  };

  const renderComments = ({item, index}) => {
    return <CommentCard item={item} />;
  };

  return (
    <View style={styles.mainContainer}>
      <PostHeader username={username} name={name} />
      <View style={styles.postContainer}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.body}>{trimBreakLine}</Text>
      </View>
      <View style={styles.searchContainer}>
        <TextInput
          onChangeText={text => onTextChange(text)}
          style={styles.searchInput}
        />
        <Image source={Search} style={styles.searchIcon} resizeMode={'cover'} />
      </View>
      {comments.length !== null ? (
        <FlatList
          data={results.length === 0 ? comments : results}
          style={styles.flatList}
          renderItem={renderComments}
          keyExtractor={(item, index) => `${index}-${item.id}`}
          // decelerationRate={0.1}
        />
      ) : (
        <ActivityIndicator color="#1DA1F2" style={styles.loader} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    height: DEVICE_HEIGHT * 0.92,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  userIcon: {
    width: DEVICE_WIDTH * 0.085,
    height: DEVICE_WIDTH * 0.085,
    marginBottom: DEVICE_HEIGHT * 0.005,
  },
  userID: {
    fontSize: DEVICE_HEIGHT * 0.015,
    fontFamily: 'Montserrat-SemiBold',
  },
  title: {
    fontSize: DEVICE_HEIGHT * 0.02,
    fontFamily: 'Montserrat-Bold',
    textAlign: 'left',
    marginTop: DEVICE_HEIGHT * 0.04,
    marginBottom: DEVICE_HEIGHT * 0.03,
  },
  body: {
    fontSize: DEVICE_HEIGHT * 0.015,
    fontFamily: 'Montserrat-Regular',
    textAlign: 'left',
    marginBottom: DEVICE_HEIGHT * 0.03,
  },
  postContainer: {
    borderBottomWidth: DEVICE_HEIGHT * 0.0008,
    borderBottomColor: 'lightgray',
    width: DEVICE_WIDTH * 1,
    paddingHorizontal: DEVICE_WIDTH * 0.04,
    marginBottom: DEVICE_HEIGHT * 0.01,
  },

  rowContainer: {
    flexDirection: 'row',
    backgroundColor: 'red',
    width: DEVICE_WIDTH * 0.9,
  },
  loader: {
    marginTop: DEVICE_HEIGHT * 0.03,
  },
  searchIcon: {
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_WIDTH * 0.05,
    tintColor: '#1DA1F2',
  },
  searchInput: {
    width: DEVICE_WIDTH * 0.8,
    height:
      Platform.OS === 'ios' ? DEVICE_HEIGHT * 0.035 : DEVICE_HEIGHT * 0.04,
    padding: 0,
  },
  searchContainer: {
    width: DEVICE_WIDTH * 0.9,
    height:
      Platform.OS === 'ios' ? DEVICE_HEIGHT * 0.035 : DEVICE_HEIGHT * 0.04,
    borderWidth: DEVICE_HEIGHT * 0.0008,
    borderColor: 'lightgray',
    borderRadius: DEVICE_WIDTH * 0.04,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});

export default Post;
