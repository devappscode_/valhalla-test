export function shuffle(array) {
  var currentIndex = array.length,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
}

export function getUserInfo(id, data) {
  for (let i = 0; i < data.length; i++) {
    if (data[i] && data[i].hasOwnProperty('id') && data[i].id === id) {
      return data[i];
    }
  }
}

export function searchPosts(inputString, users, posts) {
  if (inputString === '') {
    return [];
  }
  let results = [];
  for (let i = 0; i < users.length; i++) {
    if (
      users[i].username.toLowerCase().includes(inputString.toLowerCase()) ||
      users[i].name.toLowerCase().includes(inputString.toLowerCase())
    ) {
      for (let j = 0; j < posts.length; j++) {
        if (posts[j].userId === users[i].id) {
          results.push(posts[j]);
        }
      }
    }
  }

  for (let k = 0; k < posts.length; k++) {
    if (posts[k].title.toLowerCase().includes(inputString.toLowerCase())) {
      results.push(posts[k]);
    }
  }

  return results;
}

export function searchComments(inputString, data) {
  if (inputString === '') {
    return [];
  }
  let results = [];
  //   for (let i = 0; i < users.length; i++) {
  //     if (
  //       users[i].username.includes(inputString) ||
  //       users[i].name.includes(inputString)
  //     ) {
  //       for (let j = 0; j < posts.length; j++) {
  //         if (posts[j].userId === users[i].id) {
  //           results.push(posts[j]);
  //         }
  //       }
  //     }
  //   }

  for (let k = 0; k < data.length; k++) {
    if (
      data[k].name.toLowerCase().includes(inputString.toLowerCase()) ||
      data[k].email.toLowerCase().includes(inputString.toLowerCase()) ||
      data[k].body.toLowerCase().includes(inputString.toLowerCase())
    ) {
      results.push(data[k]);
    }
  }

  return results;
}
