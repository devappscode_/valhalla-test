const BASE_URL = 'https://jsonplaceholder.typicode.com/';
export const POSTS_URL = BASE_URL + 'posts';
export const COMMENTS_URL = BASE_URL + 'comments?postId=';
export const USERS_URL = BASE_URL + 'users';
