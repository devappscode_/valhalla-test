import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {StyleSheet, View, TouchableOpacity, Text} from 'react-native';
import {updatePosts, updateUsers} from '../redux/actions/app';
import {GET} from './service';
import {POSTS_URL, COMMENTS_URL, USERS_URL} from './urls';
import {shuffle} from '../helpers/helpers';

export async function getPosts(dispatch) {
  let config = {url: POSTS_URL};
  const response = await GET(config);
  //just to spice things up dont want to see same users again and again :D
  const shuffledResponse = shuffle(response);
  dispatch(updatePosts(shuffledResponse));
  return;
}

export async function getUsers(dispatch) {
  let config = {url: USERS_URL};
  const response = await GET(config);
  dispatch(updateUsers(response));
  return;
}

export async function getComments(postId) {
  let config = {url: COMMENTS_URL + postId};
  const response = await GET(config);
  return response;
}
