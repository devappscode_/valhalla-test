/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './app/index.js';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import React from 'react';
import configureStore from './app/redux/store/store';

const store = configureStore();

const AppRedux = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => AppRedux);
